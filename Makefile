build: lint
	go build -o sim-go main.go

lint:
	golangci-lint run
