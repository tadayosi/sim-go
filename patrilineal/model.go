package patrilineal

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const (
	maxChildren = 5
)

type Gender int

const (
	Male Gender = iota
	Female
)

type Person struct {
	Generation int
	Gender     Gender
}

func (p *Person) descend() []*Person {
	children := rand.Intn(maxChildren)
	var heirs []*Person
	for i := 0; i < children; i++ {
		child := p.breed()
		if child.Gender == Male {
			heirs = append(heirs, child)
		}
	}
	return heirs
}

func (p *Person) breed() *Person {
	return &Person{
		Generation: p.Generation + 1,
		Gender:     gender(),
	}
}

func gender() Gender {
	if rand.Intn(2) > 0 {
		return Male
	} else {
		return Female
	}
}
