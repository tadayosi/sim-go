package patrilineal

import (
	"fmt"
)

func Run() {
	fmt.Println("Patrilineal")

	gen := 0
	prim := &Person{
		Generation: gen,
		Gender:     Male,
	}
	heirs := []*Person{prim}
	for len(heirs) > 0 {
		fmt.Printf("Generation: %v,\tHeirs: %v\n", gen, len(heirs))
		next := []*Person{}
		for _, heir := range heirs {
			next = append(next, heir.descend()...)
		}
		heirs = next
		gen++
	}
}
