package cmd

import (
	"github.com/spf13/cobra"
	"github.com/tadayosi/sim-go/patrilineal"
)

func init() {
	rootCmd.AddCommand(patrilinealCmd)
}

var patrilinealCmd = &cobra.Command{
	Use:   "patrilineal",
	Short: "Patrilineal simulator",
	Long:  `Patrilineal simulator.`,
	Run: func(cmd *cobra.Command, args []string) {
		patrilineal.Run()
	},
}
