package cmd

import "github.com/spf13/cobra"

func init() {
}

var rootCmd = &cobra.Command{
	Use:   "sim-go",
	Short: "Simulation models written in Go",
	Long:  `A collection of simulation models written in Go.`,
}

func Execute() error {
	return rootCmd.Execute()
}
